package com.logineimer.randomify.repository;

import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.usecase.InternalErrorCallback;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by David Schütz on 14.03.2017.
 */
public interface RandomifyUserRepository {

  void create_or_update(RandomifyUser randomifyUser, CreateOrUpdateRandomifyUserCallback callback);

  void delete(UUID id, DeleteRandomifyUserCallback callback);

  void list(ListRandomifyUsersCallback callback);

  void findBy(String property, String value, FindRandomifyUsersCallback callback);

  interface CreateOrUpdateRandomifyUserCallback extends InternalErrorCallback {

    void created(RandomifyUser user);

    void updated(RandomifyUser user);
  }


  interface DeleteRandomifyUserCallback extends InternalErrorCallback {

    void success();
  }


  interface ListRandomifyUsersCallback extends InternalErrorCallback {

    void success(Collection<RandomifyUser> users);
  }


  interface FindRandomifyUsersCallback extends InternalErrorCallback {

    void list(Collection<RandomifyUser> users);

    void unique(RandomifyUser user);

    void empty();
  }
}
