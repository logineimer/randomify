package com.logineimer.randomify.usecase.create_user;

import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.usecase.InternalErrorCallback;
import com.logineimer.randomify.usecase.UseCaseContext;

/**
 * Created by David Schütz on 14.03.2017.
 */
public interface CreateUserUseCase {

  void execute(UseCaseContext useCaseContext, String spotifyUserId, String email,
      String displayName, String accessToken, String refreshToken, UseCaseCallback useCaseCallback);

  interface UseCaseCallback extends InternalErrorCallback {

    void success(RandomifyUser randomifyUser);
  }
}
