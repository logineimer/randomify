package com.logineimer.randomify.usecase.list_users;

import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.usecase.InternalErrorCallback;
import com.logineimer.randomify.usecase.UseCaseContext;

import java.util.Collection;

/**
 * Created by David Schütz on 14.03.2017.
 */
public interface ListUsersUseCase {

  void execute(UseCaseContext useCaseContext, ListUsersCallback listUsersCallback);

  interface ListUsersCallback extends InternalErrorCallback {

    void success(Collection<RandomifyUser> randomifyUsers);
  }
}
