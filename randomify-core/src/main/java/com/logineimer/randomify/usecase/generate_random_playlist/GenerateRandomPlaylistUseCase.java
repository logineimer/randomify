package com.logineimer.randomify.usecase.generate_random_playlist;

import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.usecase.InternalErrorCallback;
import com.logineimer.randomify.usecase.UseCaseContext;

/**
 * Created by David Schütz on 13.04.2017.
 */
public interface GenerateRandomPlaylistUseCase {

  void execute(UseCaseContext useCaseContext, RandomifyUser randomifyUser, String playlistId,
      int tracksCount, GenerateRandomPlaylistCallback generateRandomPlaylistCallback);

  interface GenerateRandomPlaylistCallback extends InternalErrorCallback {

    void success();
  }
}
