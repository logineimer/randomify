package com.logineimer.randomify.usecase.delete_user;

import com.logineimer.randomify.repository.RandomifyUserRepository;
import com.logineimer.randomify.repository.RandomifyUserRepository.DeleteRandomifyUserCallback;
import com.logineimer.randomify.usecase.UseCaseContext;

import java.util.UUID;

/**
 * Created by David Schütz on 14.03.2017.
 */
public class DeleteUserInteractor implements DeleteUserUseCase {

  private final RandomifyUserRepository randomifyUserRepository;

  public DeleteUserInteractor(final RandomifyUserRepository randomifyUserRepository) {
    this.randomifyUserRepository = randomifyUserRepository;
  }

  @Override
  public void execute(final UseCaseContext useCaseContext, final UUID id,
      final UseCaseCallback useCaseCallback) {
    randomifyUserRepository.delete(id, new DeleteRandomifyUserCallback() {

      @Override
      public void success() {
        useCaseCallback.success();
      }

      @Override
      public void internalError() {
        useCaseCallback.internalError();
      }
    });
  }
}
