package com.logineimer.randomify.usecase.generate_random_playlist;

import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.external.AuthContextDto;
import com.logineimer.randomify.external.PlaylistDto;
import com.logineimer.randomify.external.SpotifyGateway;
import com.logineimer.randomify.external.SpotifyGateway.FetchPlaylistsCallback;
import com.logineimer.randomify.external.SpotifyGateway.FetchTracksCallback;
import com.logineimer.randomify.external.SpotifyGateway.ReplacePlaylisTracksCallback;
import com.logineimer.randomify.external.TrackDto;
import com.logineimer.randomify.usecase.UseCaseContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Created by David Schütz on 13.04.2017.
 */
public class GenerateRandomPlaylistInteractor implements GenerateRandomPlaylistUseCase {

  private static final Logger LOGGER =
      Logger.getLogger(GenerateRandomPlaylistInteractor.class.getSimpleName());

  private final SpotifyGateway spotifyGateway;

  public GenerateRandomPlaylistInteractor(final SpotifyGateway spotifyGateway) {
    this.spotifyGateway = spotifyGateway;
  }

  @Override
  public void execute(final UseCaseContext useCaseContext, final RandomifyUser randomifyUser,
      final String playlistId, final int tracksCount,
      final GenerateRandomPlaylistCallback generateRandomPlaylistCallback) {
    LOGGER.info("Generate random playlist for " + randomifyUser.getSpotifyUserId());
    final AuthContextDto authContextDto =
        new AuthContextDto(randomifyUser.getSpotifyUserId(), randomifyUser.getAccessToken(),
            randomifyUser.getRefreshToken());

    final UseCaseDto useCaseDto = new UseCaseDto();
    useCaseDto.authContextDto = authContextDto;
    useCaseDto.playlistId = playlistId;
    useCaseDto.tracksCount = tracksCount;

    continueFetchingSavedTracks(useCaseDto, generateRandomPlaylistCallback);
  }

  private void continueFetchingSavedTracks(final UseCaseDto useCaseDto,
      final GenerateRandomPlaylistCallback generateRandomPlaylistCallback) {
    LOGGER.info("Fetch saved tracks...");
    spotifyGateway.fetchSavedTracks(useCaseDto.authContextDto, new FetchTracksCallback() {
      @Override
      public void success(final Collection<TrackDto> trackDtos) {
        useCaseDto.savedTracks = trackDtos;
        continueFetchingPlaylists(useCaseDto, generateRandomPlaylistCallback);
      }

      @Override
      public void internalError() {
        generateRandomPlaylistCallback.internalError();
      }
    });
  }

  private void continueFetchingPlaylists(final UseCaseDto useCaseDto,
      final GenerateRandomPlaylistCallback generateRandomPlaylistCallback) {
    LOGGER.info("Fetch playlists...");
    spotifyGateway.fetchPlaylists(useCaseDto.authContextDto, new FetchPlaylistsCallback() {
      @Override
      public void success(final Collection<PlaylistDto> playlistDtos) {
        useCaseDto.playlists = playlistDtos;
        continueFetchingPlaylistTracks(useCaseDto, generateRandomPlaylistCallback);
      }

      @Override
      public void internalError() {
        generateRandomPlaylistCallback.internalError();
      }
    });
  }

  private void continueFetchingPlaylistTracks(final UseCaseDto useCaseDto,
      final GenerateRandomPlaylistCallback generateRandomPlaylistCallback) {
    LOGGER.info("Fetch playlist tracks...");
    final Collection<TrackDto> playlistTrackDtos = new ArrayList<>();

    for (final PlaylistDto playlistDto : useCaseDto.playlists) {
      spotifyGateway.fetchPlaylistTracks(useCaseDto.authContextDto, playlistDto.id,
          new FetchTracksCallback() {
            @Override
            public void success(final Collection<TrackDto> trackDtos) {
              playlistTrackDtos.addAll(trackDtos);
            }

            @Override
            public void internalError() {
              generateRandomPlaylistCallback.internalError();
            }
          });
    }

    useCaseDto.playlistTracks = playlistTrackDtos;
    continueMergingTracks(useCaseDto, generateRandomPlaylistCallback);
  }

  private void continueMergingTracks(final UseCaseDto useCaseDto,
      final GenerateRandomPlaylistCallback generateRandomPlaylistCallback) {
    LOGGER.info("Merge tracks...");
    final List<TrackDto> allTrackDtos = new ArrayList<>();
    allTrackDtos.addAll(useCaseDto.savedTracks);
    allTrackDtos.addAll(useCaseDto.playlistTracks);
    useCaseDto.allTracks = allTrackDtos;
    continueSelectingRandomTracks(useCaseDto, generateRandomPlaylistCallback);
  }

  private void continueSelectingRandomTracks(final UseCaseDto useCaseDto,
      final GenerateRandomPlaylistCallback generateRandomPlaylistCallback) {
    LOGGER.info("Select " + useCaseDto.tracksCount + " random tracks...");
    final Random random = new Random();
    final Collection<TrackDto> tracksToUse = new ArrayList<>();

    final List<TrackDto> allTracks = useCaseDto.allTracks;
    for (int i = 0; i < useCaseDto.tracksCount; i++) {
      final int index = random.nextInt(allTracks.size());
      final TrackDto trackDto = allTracks.get(index);
      tracksToUse.add(trackDto);
      allTracks.remove(index);
    }

    useCaseDto.tracksToUse = tracksToUse;
    continuePlaylistTracksReplacement(useCaseDto, generateRandomPlaylistCallback);
  }

  private void continuePlaylistTracksReplacement(final UseCaseDto useCaseDto,
      final GenerateRandomPlaylistCallback generateRandomPlaylistCallback) {
    LOGGER.info("Replace playlist tracks...");
    spotifyGateway.replacePlaylistTracks(useCaseDto.authContextDto, useCaseDto.playlistId,
        useCaseDto.tracksToUse, new ReplacePlaylisTracksCallback() {
          @Override
          public void success() {
            generateRandomPlaylistCallback.success();
          }

          @Override
          public void internalError() {
            generateRandomPlaylistCallback.internalError();
          }
        });
  }

  private final class UseCaseDto {

    private AuthContextDto authContextDto;
    private String playlistId;
    private int tracksCount;
    private Collection<PlaylistDto> playlists;
    private List<TrackDto> allTracks;
    private Collection<TrackDto> savedTracks;
    private Collection<TrackDto> playlistTracks;
    private Collection<TrackDto> tracksToUse;
  }
}
