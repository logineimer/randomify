package com.logineimer.randomify.usecase.list_users;


import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.repository.RandomifyUserRepository;
import com.logineimer.randomify.repository.RandomifyUserRepository.ListRandomifyUsersCallback;
import com.logineimer.randomify.usecase.UseCaseContext;

import java.util.Collection;

/**
 * Created by David Schütz on 14.03.2017.
 */
public class ListUsersInteractor implements ListUsersUseCase {

  private final RandomifyUserRepository randomifyUserRepository;

  public ListUsersInteractor(final RandomifyUserRepository randomifyUserRepository) {
    this.randomifyUserRepository = randomifyUserRepository;
  }

  @Override
  public void execute(final UseCaseContext useCaseContext,
      final ListUsersCallback listUsersCallback) {
    randomifyUserRepository.list(new ListRandomifyUsersCallback() {

      @Override
      public void success(final Collection<RandomifyUser> users) {
        listUsersCallback.success(users);
      }

      @Override
      public void internalError() {
        listUsersCallback.internalError();
      }
    });
  }
}
