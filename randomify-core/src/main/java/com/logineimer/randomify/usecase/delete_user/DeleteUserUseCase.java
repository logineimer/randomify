package com.logineimer.randomify.usecase.delete_user;

import com.logineimer.randomify.usecase.InternalErrorCallback;
import com.logineimer.randomify.usecase.UseCaseContext;

import java.util.UUID;

/**
 * Created by David Schütz on 14.03.2017.
 */
public interface DeleteUserUseCase {

  void execute(UseCaseContext useCaseContext, UUID id, UseCaseCallback useCaseCallback);

  interface UseCaseCallback extends InternalErrorCallback {

    void success();
  }
}
