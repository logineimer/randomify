package com.logineimer.randomify.usecase;

import java.util.Locale;
import java.util.UUID;

/**
 * Created by David Schütz on 15.03.2017.
 */
public interface UseCaseContext {

  UUID getUserId();

  Locale getLocale();
}
