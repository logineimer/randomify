package com.logineimer.randomify.usecase;

/**
 * Created by David Schütz on 14.03.2017.
 */
public interface InternalErrorCallback {

  void internalError();
}
