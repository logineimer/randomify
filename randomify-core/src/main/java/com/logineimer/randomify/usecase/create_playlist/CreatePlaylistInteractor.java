package com.logineimer.randomify.usecase.create_playlist;

import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.external.AuthContextDto;
import com.logineimer.randomify.external.PlaylistDto;
import com.logineimer.randomify.external.SpotifyGateway;
import com.logineimer.randomify.external.SpotifyGateway.CreateSpotifyPlaylistCallback;
import com.logineimer.randomify.external.SpotifyGateway.FetchPlaylistsCallback;
import com.logineimer.randomify.usecase.UseCaseContext;

import java.util.Collection;
import java.util.logging.Logger;

/**
 * Created by David Schütz on 13.04.2017.
 */
public class CreatePlaylistInteractor implements CreatePlaylistUseCase {

  private static final Logger LOGGER =
      Logger.getLogger(CreatePlaylistInteractor.class.getSimpleName());

  private final SpotifyGateway spotifyGateway;

  public CreatePlaylistInteractor(final SpotifyGateway spotifyGateway) {
    this.spotifyGateway = spotifyGateway;
  }

  @Override
  public void execute(final UseCaseContext useCaseContext, final RandomifyUser randomifyUser,
      final String playlistName, final CreatePlaylistCallback createPlaylistCallback) {
    LOGGER.info("Create playlist '" + playlistName + "' for " + randomifyUser.getSpotifyUserId());
    final AuthContextDto authContextDto =
        new AuthContextDto(randomifyUser.getSpotifyUserId(), randomifyUser.getAccessToken(),
            randomifyUser.getRefreshToken());

    final UseCaseDto useCaseDto = new UseCaseDto();
    useCaseDto.authContextDto = authContextDto;
    useCaseDto.playlistName = playlistName;

    continueFetchingPlaylists(useCaseDto, createPlaylistCallback);
  }

  private void continueFetchingPlaylists(final UseCaseDto useCaseDto,
      final CreatePlaylistCallback createPlaylistCallback) {
    LOGGER.info("Fetch playlists...");
    spotifyGateway.fetchPlaylists(useCaseDto.authContextDto, new FetchPlaylistsCallback() {
      @Override
      public void success(final Collection<PlaylistDto> playlistDtos) {
        useCaseDto.playlists = playlistDtos;
        continueFindPlaylist(useCaseDto, createPlaylistCallback);
      }

      @Override
      public void internalError() {
        createPlaylistCallback.internalError();
      }
    });
  }

  private void continueFindPlaylist(final UseCaseDto useCaseDto,
      final CreatePlaylistCallback createPlaylistCallback) {
    LOGGER.info("Find playlist...");
    PlaylistDto playlistDto = null;
    for (PlaylistDto playlist : useCaseDto.playlists) {
      if (useCaseDto.playlistName.equals(playlist.name)) {
        playlistDto = playlist;
      }
    }

    if (playlistDto != null) {
      createPlaylistCallback.success(playlistDto);
    } else {
      continueCreatePlaylist(useCaseDto, createPlaylistCallback);
    }
  }

  private void continueCreatePlaylist(final UseCaseDto useCaseDto,
      final CreatePlaylistCallback createPlaylistCallback) {
    LOGGER.info("Create playlist...");
    spotifyGateway.createPlaylist(useCaseDto.authContextDto, useCaseDto.playlistName,
        new CreateSpotifyPlaylistCallback() {
          @Override
          public void success(final PlaylistDto playlistDto) {
            createPlaylistCallback.success(playlistDto);
          }

          @Override
          public void internalError() {
            createPlaylistCallback.internalError();
          }
        });
  }

  private final class UseCaseDto {

    private AuthContextDto authContextDto;
    private String playlistName;
    private Collection<PlaylistDto> playlists;
  }
}
