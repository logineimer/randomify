package com.logineimer.randomify.usecase.create_playlist;

import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.external.PlaylistDto;
import com.logineimer.randomify.usecase.InternalErrorCallback;
import com.logineimer.randomify.usecase.UseCaseContext;

/**
 * Created by David Schütz on 13.04.2017.
 */
public interface CreatePlaylistUseCase {

  void execute(UseCaseContext useCaseContext, RandomifyUser randomifyUser, String playlistName,
      CreatePlaylistCallback createPlaylistCallback);

  interface CreatePlaylistCallback extends InternalErrorCallback {

    void success(PlaylistDto playlistDto);
  }
}
