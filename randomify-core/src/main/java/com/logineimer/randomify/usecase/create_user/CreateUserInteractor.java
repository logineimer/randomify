package com.logineimer.randomify.usecase.create_user;

import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.repository.RandomifyUserRepository;
import com.logineimer.randomify.repository.RandomifyUserRepository.CreateOrUpdateRandomifyUserCallback;
import com.logineimer.randomify.usecase.UseCaseContext;

/**
 * Created by David Schütz on 14.03.2017.
 */
public class CreateUserInteractor implements CreateUserUseCase {

  private final RandomifyUserRepository randomifyUserRepository;

  public CreateUserInteractor(final RandomifyUserRepository randomifyUserRepository) {
    this.randomifyUserRepository = randomifyUserRepository;
  }

  @Override
  public void execute(final UseCaseContext useCaseContext, final String spotifyUserId,
      final String email, final String displayName, final String accessToken,
      final String refreshToken, final UseCaseCallback useCaseCallback) {
    RandomifyUser randomifyUser =
        new RandomifyUser(spotifyUserId, email, displayName, accessToken, refreshToken);
    randomifyUserRepository
        .create_or_update(randomifyUser, new CreateOrUpdateRandomifyUserCallback() {

          @Override
          public void created(final RandomifyUser user) {
            useCaseCallback.success(user);
          }

          @Override
          public void updated(final RandomifyUser user) {
            useCaseCallback.success(user);
          }

          @Override
          public void internalError() {
            useCaseCallback.internalError();
          }
        });
  }
}
