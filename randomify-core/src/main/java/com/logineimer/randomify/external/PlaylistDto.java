package com.logineimer.randomify.external;

/**
 * Created by David Schütz on 15.03.2017.
 */
public class PlaylistDto {

  public final String id;
  public final String href;
  public final String name;

  public PlaylistDto(final String id, final String href, final String name) {
    this.id = id;
    this.href = href;
    this.name = name;
  }
}
