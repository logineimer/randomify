package com.logineimer.randomify.external;

/**
 * Created by David Schütz on 15.03.2017.
 */
public class AuthContextDto {

  public final String userId;
  public final String accessToken;
  public final String refreshToken;

  public AuthContextDto(final String userId, final String accessToken, final String refreshToken) {
    this.userId = userId;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
  }
}
