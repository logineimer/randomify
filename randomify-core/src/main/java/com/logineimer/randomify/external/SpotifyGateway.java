package com.logineimer.randomify.external;

import com.logineimer.randomify.usecase.InternalErrorCallback;

import java.util.Collection;

/**
 * Created by David Schütz on 15.03.2017.
 */
public interface SpotifyGateway {

  /**
   * Fetch the saved tracks from the users library.
   *
   * @param authContextDto the auth context
   * @param fetchTracksCallback the callback
   */
  void fetchSavedTracks(AuthContextDto authContextDto, FetchTracksCallback fetchTracksCallback);

  /**
   * Fetch the playlist tracks.
   *
   * @param authContextDto the auth context
   * @param playlistId the playlist playlistId
   * @param fetchTracksCallback the callback
   */
  void fetchPlaylistTracks(AuthContextDto authContextDto, String playlistId,
      FetchTracksCallback fetchTracksCallback);

  /**
   * Fetch a playlist.
   *
   * @param id the playlist id
   * @param fetchPlaylistCallback the callback
   */
  void fetchPlaylist(AuthContextDto authContextDto, String id,
      FetchPlaylistCallback fetchPlaylistCallback);

  /**
   * Fetch the users playlists.
   *
   * @param fetchPlaylistsCallback the callback
   */
  void fetchPlaylists(AuthContextDto authContextDto, FetchPlaylistsCallback fetchPlaylistsCallback);

  /**
   * Create a new playlist for a user.
   *
   * @param authContextDto the auth context
   * @param name the name of the playlist
   * @param createSpotifyPlaylistCallback the callback
   */
  void createPlaylist(AuthContextDto authContextDto, String name,
      CreateSpotifyPlaylistCallback createSpotifyPlaylistCallback);

  /**
   * Replace all tracks in a playlist with another collection of tracks.
   *
   * @param authContextDto the auth context
   * @param playlistId the playlist playlistId
   * @param trackDtos the tracks to replace
   * @param replacePlaylisTracksCallback the callback
   */
  void replacePlaylistTracks(AuthContextDto authContextDto, String playlistId,
      Collection<TrackDto> trackDtos, ReplacePlaylisTracksCallback replacePlaylisTracksCallback);

  /**
   * @see #fetchSavedTracks(AuthContextDto, FetchTracksCallback)
   * @see #fetchPlaylistTracks(AuthContextDto, String, FetchTracksCallback)
   */
  interface FetchTracksCallback extends InternalErrorCallback {

    void success(Collection<TrackDto> trackDtos);
  }


  /**
   * @see #fetchPlaylist(AuthContextDto, String, FetchPlaylistCallback)
   */
  interface FetchPlaylistCallback extends InternalErrorCallback {

    void success(PlaylistDto playlistDto);
  }


  /**
   * @see #fetchPlaylists(AuthContextDto, FetchPlaylistsCallback)
   */
  interface FetchPlaylistsCallback extends InternalErrorCallback {

    void success(Collection<PlaylistDto> playlistDtos);
  }


  /**
   * @see #createPlaylist(AuthContextDto, String, CreateSpotifyPlaylistCallback)
   */
  interface CreateSpotifyPlaylistCallback extends InternalErrorCallback {

    void success(PlaylistDto playlistDto);
  }


  /**
   * @see #replacePlaylistTracks(AuthContextDto, String, Collection, ReplacePlaylisTracksCallback)
   */
  interface ReplacePlaylisTracksCallback extends InternalErrorCallback {

    void success();
  }
}
