package com.logineimer.randomify.external;

/**
 * Created by David Schütz on 15.03.2017.
 */
public class TrackDto {

  public final String id;
  public final String href;
  public final String uri;
  public final String name;

  public TrackDto(final String id, final String href, final String uri, final String name) {
    this.id = id;
    this.href = href;
    this.uri = uri;
    this.name = name;
  }
}
