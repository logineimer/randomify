package com.logineimer.randomify.entity;

import java.util.Objects;
import java.util.UUID;

/**
 * Created by David Schütz on 14.03.2017.
 */
public class RandomifyUser {

  private final UUID id;
  private final String spotifyUserId;
  private final String email;
  private final String displayName;
  private final String accessToken;
  private final String refreshToken;

  public RandomifyUser(final UUID id, final String spotifyUserId, final String email,
      final String displayName, final String accessToken, final String refreshToken) {
    this.id = id;
    this.spotifyUserId = spotifyUserId;
    this.email = email;
    this.displayName = displayName;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
  }

  public RandomifyUser(final String spotifyUserId, final String email, final String displayName,
      final String accessToken, final String refreshToken) {
    this(UUID.randomUUID(), spotifyUserId, email, displayName, accessToken, refreshToken);
  }

  public UUID getId() {
    return id;
  }

  public String getSpotifyUserId() {
    return spotifyUserId;
  }

  public String getEmail() {
    return email;
  }

  public String getDisplayName() {
    return displayName;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    final RandomifyUser that = (RandomifyUser) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return String.format(
        "%s[id=%s, spotifyUserId=%s, email=%s, displayName=%s, accessToken=%s, refreshToken=%s]",
        getClass().getSimpleName(), id, spotifyUserId, email, displayName, accessToken,
        refreshToken);
  }
}
