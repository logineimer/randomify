/**
 @license
 Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
 This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 Code distributed by Google as part of the polymer project is also
 subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import {html} from 'lit-element';
import {PageViewElement} from './page-view-element.js';
// These are the shared styles needed by this element.
import {SharedStyles} from './shared-styles.js';
import "@polymer/paper-button/paper-button";

class RdHome extends PageViewElement {
  static get styles() {
    return [
      SharedStyles
    ];
  }

  render() {
    return html`
      <section>
        <h2 class="uppercase">Deine Musik</h2>
        <p>Melde dich mit Spotify an und genieße tägliche Playlists nach deinem Geschmack.</p>
        <a href="/auth/spotify/login?state=mary"><paper-button id="login" raised>Anmelden</paper-button></a> 
      </section>
    `;
  }
}

window.customElements.define('rd-home', RdHome);
