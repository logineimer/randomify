package com.logineimer.randomify.controller.auth;

import com.google.common.base.Joiner;
import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.repository.RandomifyUserRepository;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import com.wrapper.spotify.model_objects.specification.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author dsu
 */
@Controller
@RequestMapping("/auth/spotify")
public class SpotifyAuthController {

  private static final Logger LOGGER = LoggerFactory.getLogger(SpotifyAuthController.class);

  @Autowired
  private SpotifyApi spotify;

  @Autowired
  private RandomifyUserRepository users;

  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public ModelAndView auth(@RequestParam("state") final String state) {
    final List<String> scopes = Arrays
        .asList("playlist-read-private", "playlist-read-collaborative", "playlist-modify-private",
            "playlist-modify-public", "user-library-read", "user-read-private", "user-top-read",
            "user-read-email", "user-read-birthdate");

    final URI authUrl =
        spotify.authorizationCodeUri().scope(Joiner.on(",").join(scopes)).state(state).build()
            .execute();
    return new ModelAndView("redirect:" + authUrl.toString());
  }

  @RequestMapping(value = "/callback", method = RequestMethod.GET)
  public RedirectView callback(@RequestParam("code") final String code) {
    try {
      final AuthorizationCodeCredentials authorizationCodeCredentials =
          spotify.authorizationCode(code).build().execute();

      final String accessToken = authorizationCodeCredentials.getAccessToken();
      final String refreshToken = authorizationCodeCredentials.getRefreshToken();

      spotify.setAccessToken(accessToken);
      spotify.setRefreshToken(refreshToken);
      final User user = spotify.getCurrentUsersProfile().build().execute();
      spotify.setAccessToken(null);
      spotify.setRefreshToken(null);

      handleSpotifyUser(accessToken, refreshToken, user);
    } catch (IOException | SpotifyWebApiException ignored) {
    }

    final RedirectView view = new RedirectView("/");
    view.setContextRelative(true);
    return view;
  }

  private void handleSpotifyUser(final String accessToken, final String refreshToken,
      final User spotifyUser) {
    users.findBy("spotifyUserId", spotifyUser.getId(),
        new RandomifyUserRepository.FindRandomifyUsersCallback() {

          @Override
          public void list(final Collection<RandomifyUser> users) {
            LOGGER.warn("Multiple users found for spotifyUserId = {}", spotifyUser.getId());
          }

          @Override
          public void unique(final RandomifyUser user) {
            createOrUpdate(user);
          }

          @Override
          public void empty() {
            createOrUpdate(new RandomifyUser(spotifyUser.getId(), spotifyUser.getEmail(),
                spotifyUser.getDisplayName(), accessToken, refreshToken));
          }

          @Override
          public void internalError() {
            LOGGER.error("Could not find users");
          }
        });
  }

  private void createOrUpdate(final RandomifyUser user) {
    users.create_or_update(user, new RandomifyUserRepository.CreateOrUpdateRandomifyUserCallback() {
      @Override
      public void created(final RandomifyUser user) {
        LOGGER.info("Created: " + user);
      }

      @Override
      public void updated(final RandomifyUser user) {
        LOGGER.info("Updated: " + user);
      }

      @Override
      public void internalError() {
        LOGGER.error("Could not create user");
      }
    });
  }
}
