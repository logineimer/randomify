package com.logineimer.randomify.controller.cron;

import com.logineimer.randomify.job.GenerateDailyPlaylistJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by David Schütz on 13.04.2017.
 */
@RestController
@RequestMapping("/cron")
public class CronController {

  @Autowired
  private GenerateDailyPlaylistJob generateDailyPlaylistJob;

  @RequestMapping(value = "/test", method = RequestMethod.GET)
  public ResponseEntity<String> get() {
    generateDailyPlaylistJob.execute("Test Playlist", 20);
    return ResponseEntity.status(HttpStatus.OK).build();
  }
}
