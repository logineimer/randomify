package com.logineimer.randomify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author dsu
 */
@SpringBootApplication
public class RandomifyApplication extends SpringBootServletInitializer {

  public static void main(String... args) {
//    ObjectifyService.init();
    //    ObjectifyService.register(RandomifyUserObjectifyRepository.class);
    SpringApplication.run(RandomifyApplication.class, args);
  }
}
