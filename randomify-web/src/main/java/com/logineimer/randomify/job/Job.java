package com.logineimer.randomify.job;

/**
 * Created by David Schütz on 13.04.2017.
 */
public interface Job {

  void execute(String playlistName, int tracksCount);
}
