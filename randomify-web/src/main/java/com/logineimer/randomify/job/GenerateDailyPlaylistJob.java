package com.logineimer.randomify.job;

import com.logineimer.randomify.entity.RandomifyUser;
import com.logineimer.randomify.external.PlaylistDto;
import com.logineimer.randomify.external.SpotifyGateway;
import com.logineimer.randomify.repository.RandomifyUserRepository;
import com.logineimer.randomify.usecase.UseCaseContext;
import com.logineimer.randomify.usecase.create_playlist.CreatePlaylistInteractor;
import com.logineimer.randomify.usecase.create_playlist.CreatePlaylistUseCase;
import com.logineimer.randomify.usecase.create_playlist.CreatePlaylistUseCase.CreatePlaylistCallback;
import com.logineimer.randomify.usecase.generate_random_playlist.GenerateRandomPlaylistInteractor;
import com.logineimer.randomify.usecase.generate_random_playlist.GenerateRandomPlaylistUseCase;
import com.logineimer.randomify.usecase.generate_random_playlist.GenerateRandomPlaylistUseCase.GenerateRandomPlaylistCallback;
import com.logineimer.randomify.usecase.list_users.ListUsersInteractor;
import com.logineimer.randomify.usecase.list_users.ListUsersUseCase;
import com.logineimer.randomify.usecase.list_users.ListUsersUseCase.ListUsersCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.logging.Logger;

/**
 * Created by David Schütz on 13.04.2017.
 */
@Component
public class GenerateDailyPlaylistJob implements Job {

  private static final Logger LOGGER =
      java.util.logging.Logger.getLogger(GenerateDailyPlaylistJob.class.getSimpleName());

  private final RandomifyUserRepository randomifyUserRepository;
  private final SpotifyGateway spotifyGateway;
  private final UseCaseContext useCaseContext;

  @Autowired
  public GenerateDailyPlaylistJob(final UseCaseContext useCaseContext,
      final RandomifyUserRepository randomifyUserRepository, final SpotifyGateway spotifyGateway) {
    this.useCaseContext = useCaseContext;
    this.randomifyUserRepository = randomifyUserRepository;
    this.spotifyGateway = spotifyGateway;
  }

  @Override
  public void execute(final String playlistName, final int tracksCount) {
    LOGGER.info("Executing " + getClass().getSimpleName());
    final ListUsersUseCase listUsersUseCase = new ListUsersInteractor(randomifyUserRepository);
    listUsersUseCase.execute(useCaseContext, new ListUsersCallback() {
      @Override
      public void success(final Collection<RandomifyUser> randomifyUsers) {
        LOGGER.info("Selecting all users...");
        for (final RandomifyUser randomifyUser : randomifyUsers) {
          final CreatePlaylistUseCase createPlaylistUseCase =
              new CreatePlaylistInteractor(spotifyGateway);
          createPlaylistUseCase
              .execute(useCaseContext, randomifyUser, playlistName, new CreatePlaylistCallback() {
                @Override
                public void success(final PlaylistDto playlistDto) {
                  final GenerateRandomPlaylistUseCase generateRandomPlaylistUseCase =
                      new GenerateRandomPlaylistInteractor(spotifyGateway);
                  generateRandomPlaylistUseCase
                      .execute(useCaseContext, randomifyUser, playlistDto.id, tracksCount,
                          new GenerateRandomPlaylistCallback() {
                            @Override
                            public void success() {
                              LOGGER.info("Playlist created.");
                            }

                            @Override
                            public void internalError() {
                            }
                          });
                }

                @Override
                public void internalError() {
                }
              });
        }
      }

      @Override
      public void internalError() {
      }
    });
  }
}
