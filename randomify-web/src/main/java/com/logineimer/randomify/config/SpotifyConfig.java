package com.logineimer.randomify.config;

import com.logineimer.randomify.external.SpotifyGateway;
import com.logineimer.randomify.gateway.spotify_web_api.SpotifyWebApiGateway;
import com.wrapper.spotify.SpotifyApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

/**
 * @author dsu
 */
@Configuration
public class SpotifyConfig {

  @Bean
  public SpotifyApi spotify() {
    return SpotifyApi.builder().setClientId("562288dd95d34781bb5ee8fa7c3c6349")
        .setClientSecret("6f8fc343b103418e85c28f0bb664365d")
        .setRedirectUri(URI.create("http://localhost:8080/auth/spotify/callback")).build();
  }

  @Bean
  public SpotifyGateway spotifyGateway() {
    return new SpotifyWebApiGateway(spotify());
  }
}
