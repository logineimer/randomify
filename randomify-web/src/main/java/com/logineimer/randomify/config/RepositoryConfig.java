package com.logineimer.randomify.config;

import com.logineimer.randomify.repository.RandomifyUserDatastoreRepository;
import com.logineimer.randomify.repository.RandomifyUserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author dsu
 */
@Configuration
public class RepositoryConfig {

  @Bean
  public RandomifyUserRepository userRepository() {
    return new RandomifyUserDatastoreRepository();
  }
}
