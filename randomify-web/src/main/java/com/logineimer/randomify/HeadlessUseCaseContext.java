package com.logineimer.randomify;

import com.logineimer.randomify.usecase.UseCaseContext;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.UUID;

/**
 * Created by David Schütz on 13.04.2017.
 */
@Component
public class HeadlessUseCaseContext implements UseCaseContext {

  @Override
  public UUID getUserId() {
    return null;
  }

  @Override
  public Locale getLocale() {
    return Locale.GERMANY;
  }
}
