package com.logineimer.randomify.repository;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.KeyFactory;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.QueryResults;
import com.google.cloud.datastore.StructuredQuery;
import com.logineimer.randomify.entity.DatastoreRandomifyUser;
import com.logineimer.randomify.entity.RandomifyUser;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author dsu
 */
public class RandomifyUserDatastoreRepository implements RandomifyUserRepository {

  private static final String KIND = "User";

  @Override
  public void create_or_update(final RandomifyUser user,
      final CreateOrUpdateRandomifyUserCallback callback) {
    final Datastore store = DatastoreOptions.getDefaultInstance().getService();
    final KeyFactory factory = store.newKeyFactory().setKind(KIND);
    final DatastoreRandomifyUser data = DatastoreRandomifyUser.from(user);
    final Key key = data.key(factory);

    Entity entity = store.get(key);
    if (entity != null) {
      store.update(entity);
      callback.updated(user);
    } else {
      entity = data.toEntity(key);
      store.put(entity);
      callback.created(user);
    }
  }

  @Override
  public void delete(final UUID id, final DeleteRandomifyUserCallback callback) {
    final Datastore store = DatastoreOptions.getDefaultInstance().getService();
    final KeyFactory factory = store.newKeyFactory().setKind(KIND);
    store.delete(factory.newKey(id.toString()));
    callback.success();
  }

  @Override
  public void list(final ListRandomifyUsersCallback callback) {
    final Datastore store = DatastoreOptions.getDefaultInstance().getService();
    final Query<Entity> query = Query.newEntityQueryBuilder().setKind(KIND).build();
    final QueryResults<Entity> data = store.run(query);
    final List<RandomifyUser> result = new ArrayList<>();
    data.forEachRemaining(entity -> {
      final DatastoreRandomifyUser user = DatastoreRandomifyUser.from(entity);
      result.add(new RandomifyUser(user.getId(), user.getSpotifyUserId(), user.getEmail(),
          user.getDisplayName(), user.getAccessToken(), user.getRefreshToken()));
    });
    callback.success(result);
  }

  @Override
  public void findBy(final String property, final String value,
      final FindRandomifyUsersCallback callback) {
    final Datastore store = DatastoreOptions.getDefaultInstance().getService();
    final Query<Entity> query = Query.newEntityQueryBuilder().setKind(KIND)
        .setFilter(StructuredQuery.PropertyFilter.eq(property, value)).build();
    final QueryResults<Entity> data = store.run(query);
    final List<RandomifyUser> result = new ArrayList<>();
    data.forEachRemaining(entity -> {
      final DatastoreRandomifyUser user = DatastoreRandomifyUser.from(entity);
      result.add(new RandomifyUser(user.getId(), user.getSpotifyUserId(), user.getEmail(),
          user.getDisplayName(), user.getAccessToken(), user.getRefreshToken()));
    });
    if (result.isEmpty()) {
      callback.empty();
    } else if (result.size() == 1) {
      callback.unique(result.get(0));
    } else {
      callback.list(result);
    }
  }
}
