package com.logineimer.randomify.entity;

import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.KeyFactory;

import java.util.Objects;
import java.util.UUID;

/**
 * @author dsu
 */
@SuppressWarnings("squid:TrailingComment")
public class DatastoreRandomifyUser {

  private final UUID id;

  private String spotifyUserId;

  private String email;

  private String displayName;

  private String accessToken;

  private String refreshToken;

  private DatastoreRandomifyUser(final UUID id) {
    Objects.requireNonNull(id);
    this.id = id;
  }

  public Key key(final KeyFactory factory) {
    return factory.newKey(getId().toString());
  }

  public Entity toEntity(final Key key) {
    return Entity.newBuilder(key) //
        .set("spotifyUserId", getSpotifyUserId()) //
        .set("email", getEmail()) //
        .set("displayName", getDisplayName()) //
        .set("accessToken", getAccessToken()) //
        .set("refreshToken", getRefreshToken()) //
        .build();
  }

  public UUID getId() {
    return id;
  }

  public String getSpotifyUserId() {
    return spotifyUserId;
  }

  public void setSpotifyUserId(final String spotifyUserId) {
    this.spotifyUserId = spotifyUserId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(final String displayName) {
    this.displayName = displayName;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(final String accessToken) {
    this.accessToken = accessToken;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public void setRefreshToken(final String refreshToken) {
    this.refreshToken = refreshToken;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    final DatastoreRandomifyUser that = (DatastoreRandomifyUser) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return String.format(
        "%s[id=%s, spotifyUserId=%s, email=%s, displayName=%s, accessToken=%s, refreshToken=%s]",
        getClass().getSimpleName(), id, spotifyUserId, email, displayName, accessToken,
        refreshToken);
  }

  public static DatastoreRandomifyUser from(final RandomifyUser user) {
    Objects.requireNonNull(user);
    final DatastoreRandomifyUser result = new DatastoreRandomifyUser(user.getId());
    result.setSpotifyUserId(user.getSpotifyUserId());
    result.setEmail(user.getEmail());
    result.setDisplayName(user.getDisplayName());
    result.setAccessToken(user.getAccessToken());
    result.setRefreshToken(user.getRefreshToken());
    return result;
  }

  public static DatastoreRandomifyUser from(final Entity entity) {
    Objects.requireNonNull(entity);
    final UUID id = UUID.fromString(entity.getKey().getName());
    final DatastoreRandomifyUser result = new DatastoreRandomifyUser(id);
    result.setSpotifyUserId(entity.getString("spotifyUserId"));
    result.setEmail(entity.getString("email"));
    result.setDisplayName(entity.getString("displayName"));
    result.setAccessToken(entity.getString("accessToken"));
    result.setRefreshToken(entity.getString("refreshToken"));
    return result;
  }
}
