package com.logineimer.randomify.gateway.spotify_web_api.util;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Paging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by David Schütz on 14.03.2017.
 */
public class SpotifyAllHelper {

  private static final int LIMIT = 30;

  public static <E> List<E> collect(final SpotifyApi api, final SpotifyObjectSupplier<E> supplier)
      throws IOException, SpotifyWebApiException {
    final List<E> result = new ArrayList<>();
    int fetchSize;
    int offset = 0;

    do {
      final Paging<E> page = supplier.supply(api, LIMIT, offset);
      final E[] next = page.getItems();
      fetchSize = next.length;
      offset += fetchSize;

      result.addAll(Arrays.asList(next));
    } while (LIMIT == fetchSize);

    return result;
  }

  @FunctionalInterface
  public interface SpotifyObjectSupplier<E> {

    Paging<E> supply(SpotifyApi api, int limit, int offset)
        throws IOException, SpotifyWebApiException;
  }
}
