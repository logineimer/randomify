package com.logineimer.randomify.gateway.spotify_web_api;

import com.logineimer.randomify.external.AuthContextDto;
import com.logineimer.randomify.external.PlaylistDto;
import com.logineimer.randomify.external.SpotifyGateway;
import com.logineimer.randomify.external.TrackDto;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Playlist;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.model_objects.specification.SavedTrack;
import com.wrapper.spotify.model_objects.specification.Track;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.logineimer.randomify.gateway.spotify_web_api.util.SpotifyAllHelper.collect;

/**
 * Implements spotify-web-api by thelinmichael.
 * <p>
 * Created by David Schütz on 15.03.2017.
 */
public class SpotifyWebApiGateway implements SpotifyGateway {

  private final SpotifyApi spotify;

  public SpotifyWebApiGateway(final SpotifyApi spotify) {
    this.spotify = spotify;
  }

  @Override
  public void fetchSavedTracks(final AuthContextDto authContextDto,
      final FetchTracksCallback fetchTracksCallback) {
    try {
      setAuthContext(authContextDto);
      final Collection<SavedTrack> libraryTracks = collect(spotify,
          (api, limit, offset) -> api.getUsersSavedTracks().limit(limit).offset(offset).build()
              .execute());
      invalidateAuthContext();

      Collection<TrackDto> trackDtos = new ArrayList<>();
      for (SavedTrack libraryTrack : libraryTracks) {
        final Track track = libraryTrack.getTrack();
        trackDtos
            .add(new TrackDto(track.getId(), track.getHref(), track.getUri(), track.getName()));
      }
      fetchTracksCallback.success(trackDtos);
    } catch (IOException | SpotifyWebApiException e) {
      invalidateAuthContext();
      fetchTracksCallback.internalError();
    }
  }

  @Override
  public void fetchPlaylistTracks(final AuthContextDto authContextDto, final String playlistId,
      final FetchTracksCallback fetchTracksCallback) {
    try {
      setAuthContext(authContextDto);
      final Collection<PlaylistTrack> playlistTracks = collect(spotify,
          (api, limit, offset) -> api.getPlaylistsTracks(authContextDto.userId, playlistId)
              .limit(limit).offset(offset).build().execute());
      invalidateAuthContext();

      Collection<TrackDto> trackDtos = new ArrayList<>();
      for (PlaylistTrack playlistTrack : playlistTracks) {
        final Track track = playlistTrack.getTrack();
        trackDtos
            .add(new TrackDto(track.getId(), track.getHref(), track.getUri(), track.getName()));
      }
      fetchTracksCallback.success(trackDtos);
    } catch (IOException | SpotifyWebApiException e) {
      invalidateAuthContext();
      fetchTracksCallback.internalError();
    }
  }

  @Override
  public void fetchPlaylist(final AuthContextDto authContextDto, final String id,
      final FetchPlaylistCallback fetchPlaylistCallback) {
    try {
      setAuthContext(authContextDto);
      final Playlist playlist = spotify.getPlaylist(authContextDto.userId, id).build().execute();
      invalidateAuthContext();
      fetchPlaylistCallback
          .success(new PlaylistDto(playlist.getId(), playlist.getHref(), playlist.getName()));
    } catch (IOException | SpotifyWebApiException e) {
      invalidateAuthContext();
      fetchPlaylistCallback.internalError();
    }
  }

  @Override
  public void fetchPlaylists(final AuthContextDto authContextDto,
      final FetchPlaylistsCallback fetchPlaylistsCallback) {
    try {
      setAuthContext(authContextDto);
      final Collection<PlaylistSimplified> playlists = collect(spotify,
          (api, limit, offset) -> api.getListOfUsersPlaylists(authContextDto.userId).limit(limit)
              .offset(offset).build().execute());
      invalidateAuthContext();

      final Collection<PlaylistDto> playlistDtos = new ArrayList<>();
      for (final PlaylistSimplified playlist : playlists) {
        playlistDtos.add(new PlaylistDto(playlist.getId(), playlist.getHref(), playlist.getName()));
      }
      fetchPlaylistsCallback.success(playlistDtos);
    } catch (IOException | SpotifyWebApiException e) {
      invalidateAuthContext();
      fetchPlaylistsCallback.internalError();
    }
  }

  @Override
  public void createPlaylist(final AuthContextDto authContextDto, final String name,
      final CreateSpotifyPlaylistCallback createSpotifyPlaylistCallback) {
    try {
      setAuthContext(authContextDto);
      final Playlist playlist =
          spotify.createPlaylist(authContextDto.userId, name).public_(false).build().execute();
      invalidateAuthContext();

      final PlaylistDto playlistDto =
          new PlaylistDto(playlist.getId(), playlist.getHref(), playlist.getName());
      createSpotifyPlaylistCallback.success(playlistDto);
    } catch (IOException | SpotifyWebApiException e) {
      invalidateAuthContext();
      createSpotifyPlaylistCallback.internalError();
    }
  }

  @Override
  public void replacePlaylistTracks(final AuthContextDto authContextDto, final String playlistId,
      final Collection<TrackDto> trackDtos,
      final ReplacePlaylisTracksCallback replacePlaylisTracksCallback) {
    final List<String> trackUris = new ArrayList<>();
    for (final TrackDto dto : trackDtos) {
      trackUris.add(dto.uri);
    }

    try {
      setAuthContext(authContextDto);
      spotify.replacePlaylistsTracks(authContextDto.userId, playlistId,
          trackUris.toArray(new String[0])).build().execute();

      invalidateAuthContext();
      replacePlaylisTracksCallback.success();
    } catch (IOException | SpotifyWebApiException e) {
      invalidateAuthContext();
      replacePlaylisTracksCallback.internalError();
    }
  }

  private void setAuthContext(final AuthContextDto authContextDto) {
    spotify.setAccessToken(authContextDto.accessToken);
    spotify.setRefreshToken(authContextDto.refreshToken);
  }

  private void invalidateAuthContext() {
    spotify.setAccessToken(null);
    spotify.setRefreshToken(null);
  }
}
