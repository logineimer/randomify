#!/bin/sh

verifyArgs() {
  if [ $# -ne 1 ] ; then
    if [ $# -gt 1 ]
      then
        echo "[ERROR] It seems you provided more than one argument for this script. Please enter only the version you wish to update to, sir."
      else
        echo "[ERROR] It seems you provided no arguments for this script. Please enter the version you wish to update to, sir."
    fi
    exit 1
  fi
}

fail() {
  echo "[FATAL] Error while updating the project version. Please fix your build, sir."
  exit 1
}

verifyArgs $@

# Set versions
mvn versions:set -DnewVersion=$1

if [ $? -ne 0 ] ; then
  fail
fi

# Commit versions update
mvn versions:commit

if [ $? -ne 0 ] ; then
  fail
fi

echo -e "[SUCCESS] Successfully updated the project version to $1. It's been a pleasure to see you, sir.\n"
git status
exit 0
